# Device manageing Rest API W. Go & Gin


## Introduction

It has been a while for me to develop actor to handle high scalable concurrent requests. Considering some issues we are facing in projects, like Scala upgrade caused library conflicting issue, AKKA licencing kicking off, I have been thinking about other alternative. 


Here comes the demo opportunity and I am happy to take challenge to build a Go project with Gin, for the first time.



## High Level Requirements

[Here](./docs/Arrcus-Go_backend-Design_and_code.pdf) is the doc


## Research and impl Roadmap

### What I need to pick up before kicking off implementation
A. Go lang, which is almost new to me. Which features will be key to pick up for this assigment
B. Rest api framework base on Go lang
C. Any Go lang based ORM (Postgre DB )


Go's solution to high concurrent issue is Goroutine, one sub type of Coroutine (with recent active develop in Kotlin). Gin's impl counts on Goroutine for high volumn requests. Gin  serves each request by an individual goroutine. We provide the [Performance demo](## Performance test proved Gin, built on goroutine framework, can handle hight volumn of requests) 

### Data Persisting & Access framework

For time constraint the data storage / persistent layers have been implemented quick and simple. postgres combined with gorm for ORM. But I would like to abstract data component as service to make it nimble for all other different kind of solutions besides entity relationship based SQL DB.

Schema definition (ignore if already exists) is through [init_tool.go](./tools/init_tool.go)

In the impl some hiccups happened for ORM on handling postgres featured type like Point and CIDR. Here is the quick schema and defining more indexes through ORM schema creating.

Customer entity now ignored with some customer assigned column here only.
```
postgres=# \d+ devices;
                                                               Table "public.devices"
       Column        |           Type           | Collation | Nullable |               Default               | Storage  | Stats target | Description 
---------------------+--------------------------+-----------+----------+-------------------------------------+----------+--------------+-------------
 id                  | bigint                   |           | not null | nextval('devices_id_seq'::regclass) | plain    |              | 
 serial_id           | text                     |           |          |                                     | extended |              | 
 latitude            | real                     |           |          |                                     | plain    |              | 
 longitude           | real                     |           |          |                                     | plain    |              | 
 claimed_customer_id | bigint                   |           |          |                                     | plain    |              | 
 created_at          | timestamp with time zone |           |          | now()                               | plain    |              | 
 updated_at          | timestamp with time zone |           |          | now()                               | plain    |              | 
Indexes:
    "devices_pkey" PRIMARY KEY, btree (id)
```



### Some ref docs used for knowledge pick up:

Go install: https://go.dev/doc/tutorial/getting-started


Gin server: https://go.dev/doc/tutorial/web-service-gin


## Launch and run

### How to run

Create and populate simple data through [init_tool.go](./tools/init_tool.go). exec `go run .` under that path

Go to project root path and find `ctl/demoCtl.go`, run `go get .`, `go run .`

### Api samples with demo

Here is the [demo](./docs/demo/Api_demo.mov) for apis. First 3 prove api calls. The last one is to trigger 100 concurrent calls each running on its Coroutins. As observed this scenario Gin handle it at ~100µs. 

Here is the curls samples for apis defined

```curl http://localhost:8080/customer/devices \
    --include \
    --header "Content-Type: application/json" \
    --request "POST" \
    --data '{"customer_id":"771778", "action": "assign","device": "xyz_98788"}'
```

`curl "localhost:8080/devices?assigned=true"`

`curl "localhost:8080/customer/771778/devices"`

### More topics yet to impl
- API doc by integrating with SWAGGER
- More tests and integration tests at Gin-gonic level
- Data modeling expand like customers
- GORM issues, like indexings.
- Gin 

## Some Meditaions and open discussions

After finishing the implementation, I have been trying to pull out my perspective 3kfeet above and rebrewing pro and cons of solutions. 

Basically Go lang's attactive feature in handling the " millions of requests " challenging, is the Goroutine pattern. However, the initial research through Go/Gin doc I explored (I might be wrong) bring me several concerns:

- Go is basically developed kind of like C/C++ which has little accomodations when coming to supporing packages and open source projects supporting, even IDE choose is very limited.

- This brings me to turn back to Kotlin Coroutine. It is because Coroutines in Kotlin is interoperably supporting JVM, making its more supportive from IDE to all the developemnt needed frameworks, including application framework like Spring, server like Spring boots, and wide choice of testing framework. Spring Boot is now suppring running Coroutine based Kotlin app, which is design to handle the high volumn request on server in async way.


## Performance test proved Gin, built on goroutine framework, can handle hight volumn of requests

Test scripts are provided here [api_concurrent.go](../test/performance/api_concurrent.go) . Below are the log on gin showing we can demo how fast gin framework can handle it well. Other than initial call possibly due to resource launching, normall concurrent calls are handled at micro seconds level.

```
[GIN-debug] Listening and serving HTTP on localhost:8080
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |    9.402791ms |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     180.583µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     148.375µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     130.792µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     121.583µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     126.333µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |       150.5µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     136.042µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     164.334µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     130.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     137.917µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     130.708µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |         122µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     120.042µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      124.75µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      116.75µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |         120µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      114.25µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     124.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     115.459µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     120.958µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     118.917µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     120.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     129.125µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     115.166µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     104.292µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     112.167µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     103.375µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     109.958µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     118.542µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     108.541µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     149.375µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     118.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     109.541µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     116.208µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     114.375µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     118.625µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      95.583µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     116.541µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     102.042µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     111.958µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     103.208µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     113.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      95.416µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     115.416µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     112.292µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      109.25µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     108.792µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     119.958µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     124.667µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      104.75µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     108.959µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |       131.5µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     118.666µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     114.959µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     121.375µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     104.417µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     128.417µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     115.125µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     101.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     120.208µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     106.834µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     111.375µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     111.583µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |       139.5µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     127.292µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     109.625µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     140.959µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     116.292µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      108.25µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |         106µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     100.542µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     125.292µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     122.334µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     116.417µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      95.833µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     122.708µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     109.667µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     104.083µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     109.792µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     100.041µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      119.25µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     106.417µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     104.583µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      98.667µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     106.417µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      122.75µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     117.875µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     145.458µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     142.208µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     105.708µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     165.042µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     166.208µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     111.792µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      98.459µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     128.583µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      98.083µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      105.75µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |      101.75µs |       127.0.0.1 | GET      "/devices?assigned=true"
[{0 abc_123 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC} {0 xyz_98788 0 0 0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC}]
[GIN] 2022/11/20 - 18:53:24 | 200 |     109.791µs |       127.0.0.1 | GET      "/devices?assigned=true"

```

