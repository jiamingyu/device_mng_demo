package main

import (
    "log"

	"jyu/device_mng_demo/config"
   "jyu/device_mng_demo/datamodel"

	orm "github.com/go-pg/pg/v9/orm"
   "github.com/go-pg/pg/v9"
)


func main() {

   log.Printf("starting creating tables ...")

   var db *pg.DB = config.Connect()

   opts := &orm.CreateTableOptions{
      IfNotExists: true,
   }

   createError := db.CreateTable(&datamodel.Device{}, opts)

   if createError != nil {
      log.Printf("Error while creating tables, Reason: %v\n", createError)
   }
   log.Printf("Tables created")


   // Demo insert
   insertError := db.Insert(&datamodel.Device{
      SerialId: "abc_123",
      Latitude: 122.23,
      Longitude: -108.78,
      ClaimedCustomerId: 19007,
   } )

   insertError = db.Insert(&datamodel.Device{
         SerialId: "xyz_98788",
         Latitude: -202.23,
         Longitude: 88.78,
         ClaimedCustomerId: 19007,
   } )

   if insertError != nil {
      log.Printf("Error while inserting new rec into db, Reason: %v\n", insertError)
   }

}

