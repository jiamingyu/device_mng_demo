package main

import (
    "net/http"
    "log"
    "fmt"
    "strconv"
    "reflect"
    // "time"

    "jyu/device_mng_demo/config"
   // "jyu/device_mng_demo/datamodel"
   "jyu/device_mng_demo/service"

    "github.com/gin-gonic/gin"
    "github.com/go-pg/pg/v9"

   // "github.com/jackc/pgtype"


)

type ReserveDeviceRequest struct {
    CustomerId string `json:"customer_id"`
    Action string `json:"action"`
    Device string
}

type ReserveDeviceResonse struct {
    CustomerId string `json:"customer_id"`
    Action string `json:"action"`
    Succeeded bool `json:"succeeded"`

}


func main() {
    var db *pg.DB = config.Connect()
    service.InitiateDB(db)


    router := gin.Default()
    // get col of reserved/released devices    
    router.GET("/devices", getDevices)    
    // get all devices reserved by customer
    router.GET("/customer/:customerId/devices", getCustomerAssignedDevices)         
    // post reserve/release devices
    router.POST("/customer/devices",updateCustomerAssignedDevices)

    router.Run("localhost:8080")
}

func getDevices(c *gin.Context) {
    var filter = c.DefaultQuery("assigned", "false")
    // todo: limit, offset

    var isAssigned, _ = strconv.ParseBool(filter)

    c.IndentedJSON(http.StatusOK, service.GetPaginatedDevices(isAssigned)) 
}

func getCustomerAssignedDevices(c *gin.Context) {
     var custId int
     custId, _ = strconv.Atoi(c.Param("customerId"))
     fmt.Println(custId, reflect.TypeOf(custId))  //debug

     c.IndentedJSON(http.StatusOK, service.GetDevicesAssignedTo(custId))
}

func updateCustomerAssignedDevices(c *gin.Context) {
    var reserveRequest   ReserveDeviceRequest
    err := c.BindJSON(&reserveRequest)
    if err != nil {
            log.Fatal(err)
        }
    fmt.Println(reserveRequest)

    custId, _ := strconv.Atoi(reserveRequest.CustomerId)

    service.ReserveOrRelease(reserveRequest.Device, custId)

    var reserveDeviceResonse ReserveDeviceResonse
    reserveDeviceResonse.CustomerId = reserveRequest.CustomerId

    c.IndentedJSON(http.StatusOK, reserveDeviceResonse)
}
