package main

import (
   "io/ioutil"
   "log"
   "net/http"
   "sync"
)


func main() {

   callCounts := 100

   log.Printf("starting test ...")

   
	wg := sync.WaitGroup{}



	for i :=0;i<callCounts;i++ {

		wg.Add(1)

      resp, err := http.Get("http://localhost:8080/devices?assigned=true")

      if err != nil {
         log.Fatalln(err)
      }

      body, err := ioutil.ReadAll(resp.Body)
      if err != nil {
         log.Fatalln(err)
      }
      //Convert the body to type string
      sb := string(body)
      log.Printf(sb)

      wg.Done()



   }






	

}