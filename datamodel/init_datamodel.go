package datamodel

import (
    "time"
)

type Device struct {
   Id       int `gorm:"primary_key, AUTO_INCREMENT"`
   SerialId        string    `gorm:"uniqueIndex" json:"serial_id"`
   Latitude  float32 `json: "latitude"`
   Longitude  float32 `json: "longitude"`
   ClaimedCustomerId int      `gorm:"uniqueIndex" json:"claimed_customer_id"`

   CreatedAt time.Time `pg:"default:now()" json:"created_at"`
   UpdatedAt time.Time `pg:"default:now()" json:"updated_at"`
}
