package service

import (

  "log"
  "fmt"

  "jyu/device_mng_demo/datamodel"

  "github.com/go-pg/pg/v9"

)

var demos2 = []datamodel.Device{
    datamodel.Device{
      SerialId: "abc",
      Latitude: 122.23,
      Longitude: -108.78,
      ClaimedCustomerId: 19007,
   },
}

var dbConnect *pg.DB
func InitiateDB(db *pg.DB) {
  dbConnect = db
}


func GetPaginatedDevices(isAssigned bool) []datamodel.Device {
  var devices []datamodel.Device

  var whereClause string

  if (isAssigned) {
    whereClause = "claimed_customer_id is not null"
  } else {
    whereClause = "claimed_customer_id is null"
  }

  err := dbConnect.Model(&devices).ColumnExpr("serial_id").
    Where(whereClause).Select()

  if err != nil {
    log.Printf("go wrong when query devices")

  }

  fmt.Println(devices)

  return devices
}

func GetDevicesAssignedTo(custId int) []datamodel.Device  {
  var devices []datamodel.Device
  err := dbConnect.Model(&devices).//ColumnExpr("serial_id").
    Where("claimed_customer_id = ?", custId).Select()

  if err != nil {
    log.Printf("go wrong when query devices")

  }

  fmt.Println(devices)

  return devices

}



func ReserveOrRelease(serialId string, custId int) map[string] []int {

  r := make(map[string][]int)

  // first check device exists
  var devices []datamodel.Device
  err := dbConnect.Model(&devices).Where("serial_id = ?", serialId).Select()
  if err != nil {
        fmt.Println("something wrong ... ...:", err)
  }

  dv := devices[0]
  dv.ClaimedCustomerId = custId

  dv2, err2 := dbConnect.Model(&dv).Where("serial_id = ?", serialId).Update()

  if err2 != nil {
    fmt.Println("something wrong 22 ... ...:", err2, dv2)
  }

  return r
}
